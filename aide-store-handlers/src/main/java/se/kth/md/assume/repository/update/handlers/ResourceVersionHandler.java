package se.kth.md.assume.repository.update.handlers;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.codec.digest.DigestUtils;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.Link;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.lyo.tools.store.ModelUnmarshallingException;
import org.eclipse.lyo.tools.store.Store;
import org.eclipse.lyo.tools.store.StoreAccessException;
import org.eclipse.lyo.tools.store.update.Handler;
import org.eclipse.lyo.tools.store.update.ServiceProviderMessage;
import org.eclipse.lyo.tools.store.update.change.Change;
import org.eclipse.lyo.tools.store.update.change.ChangeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.aide.oslc.ccm.VersionResource;
import se.kth.md.aide.oslc.ccm.VersionServiceConfiguration;
import se.kth.md.aide.repository.api.Repository;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.assume.repository.update.RevisionMessage;

/**
 * ResourceVersionHandler is .
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-09-19
 */
public class ResourceVersionHandler<T extends ServiceProviderMessage & RevisionMessage>
    implements Handler<T> {

  private final static Logger log = LoggerFactory.getLogger(ResourceVersionHandler.class);
  private final Map<URI, ServiceProvider> serviceProviderMap = new HashMap<>();
  private final Repository repository;
  private String servicesBase;

  public ResourceVersionHandler(ServiceProvider[] serviceProviders, Repository repository,
      final String servicesBase) {
    this.repository = repository;
    this.servicesBase = servicesBase;
    for (ServiceProvider serviceProvider : serviceProviders) {
      log.info("Registering Service Provider '{}' for versioning", serviceProvider);
      serviceProviderMap.put(serviceProvider.getAbout(), serviceProvider);
    }
  }

  @Override
  public void handle(Store store, Collection<Change<T>> changes) {
    Map<URI, List<Change<T>>> changesRevisionSP = changes.stream()
        .collect(Collectors.groupingBy(o -> o.getMessage().getServiceProviderId()));

    for (Map.Entry<URI, List<Change<T>>> uriListEntry : changesRevisionSP.entrySet()) {
      URI spGraphKey = uriListEntry.getKey();
      URI versionsGraph = VersionServiceConfiguration.getVersionResourceGraphName(spGraphKey);
      List<Change<T>> changesCreated = ChangeHelper.changesCreated(uriListEntry.getValue());
      List<Change<T>> changesModified = ChangeHelper.changesModified(uriListEntry.getValue());
      try {
        final Collection<VersionResource> created = versionsForCreated(changesCreated);
        appendOrPutResources(store, versionsGraph, created);
        updateModifiedVersions(changesModified, versionsGraph, store);
      } catch (Exception e) {
        log.error(String.valueOf(e));
      }
    }
  }

  private boolean appendOrPutResources(final Store store, final URI graph,
      final Iterable<? extends AbstractResource> resources) {
    try {
      ImmutableList<AbstractResource> copyOf = ImmutableList.copyOf(resources);
      for (AbstractResource resource : copyOf) {
        log.debug("Inserting: {}", resource);
      }
      return store.updateResources(graph, copyOf.toArray(new AbstractResource[0]));
    } catch (Exception e) {
      log.error("Store exception", e);
      return false;
    }
  }

  private Collection<VersionResource> versionsForCreated(Collection<Change<T>> createdChanges) {
    return createdChanges.stream()
        .map(r -> buildVersionResource(r.getMessage().getRevision(),
            r.getMessage().getServiceProviderId(), r.getResource()))
        .collect(Collectors.toList());
  }

  private VersionResource buildVersionResource(Revision revision, URI serviceProvider,
      AbstractResource r) {
    log.debug("Building a VR for {}@{}:{}", r, serviceProvider, revision);
    try {
      String urlHash = DigestUtils.md5Hex(r.getAbout().toString());
      String revisionId = revision.getId();
      if (serviceProvider != null) {
        final ServiceProvider provider = serviceProviderMap.get(serviceProvider);
        if (provider != null) {
          VersionResource versionResource = new VersionResource(provider.getIdentifier(), urlHash,
              revisionId, servicesBase);
          versionResource.setIdentifier(urlHash);
          versionResource.setVersionId(revisionId);
          versionResource.setWasRevisionOf(Sets.newHashSet(new Link(r.getAbout(), r.toString())));
          versionResource.setCreated(Date.from(revision.getTimestamp().toInstant()));
          return versionResource;
        } else {
          // IllegalArgumentException is thrown below
          // TODO Andrew@2017-04-04: do we need to allow this case?
          log.warn("Service Provider '{}' is not mapped for versioning", serviceProvider);
        }
      } else {
        // IllegalArgumentException is thrown below
        log.error("Service Provider must not be NULL");
      }
    } catch (URISyntaxException e) {
      log.warn("Some URIs related to the Version Resource or its CR are broken", e);
    }

    throw new IllegalArgumentException("Cannot construct a valid VersionResource");
  }

  private void updateModifiedVersions(Collection<Change<T>> modifiedChanges,
      final URI versionsGraphKey, final Store store) throws StoreAccessException {
    List<VersionResource> versionResources = new ArrayList<>();

    Map<Revision, List<Change<T>>> revisionListMap = modifiedChanges.stream()
        .collect(Collectors.groupingBy(o -> o.getMessage().getRevision()));

    List<Revision> revisions = new ArrayList<>(revisionListMap.keySet());
    revisions.sort(Comparator.comparing(Revision::getTimestamp)); // hope it's the ascending order

    // TODO /Andrew 21.03.17: sort all revisions always
    List<Revision> allRevisions = repository.getRevisions();
    revisions.sort(Comparator.comparing(Revision::getTimestamp));

    for (Revision revision : revisions) {
      List<Change<T>> changeList = revisionListMap.get(revision);
      int i = allRevisions.indexOf(revision);
      if (i > 0) {
        List<VersionResource> resources = getVersionResourcesForKey(versionsGraphKey, store);

        Map<URI, VersionResource> versionResourceMap = new HashMap<>();
        for (VersionResource resource : resources) {
          URI uri = Iterables.getOnlyElement(resource.getWasRevisionOf()).getValue();
          if (versionResourceMap.putIfAbsent(uri, resource) != null) {
            if (versionResourceMap.get(uri).getCreated().before(resource.getCreated())) {
              versionResourceMap.put(uri, resource);
            }
          }

        }

        List<VersionResource> versionResourceList = changeList.stream()
            .map(r -> buildVersionResource(r.getMessage().getRevision(),
                r.getMessage().getServiceProviderId(), r.getResource()))
            .collect(Collectors.toList());

        for (VersionResource versionResource : versionResourceList) {
          URI about = Iterables.getOnlyElement(versionResource.getWasRevisionOf()).getValue();
          if (versionResourceMap.containsKey(about)) {
            VersionResource versionResourceOld = versionResourceMap.get(about);
            versionResource.addWasDerivedFrom(
                new Link(versionResourceOld.getAbout(), versionResource.toString()));
          }
        }

        versionResources.addAll(versionResourceList);
      } else {
        List<VersionResource> versionResourceList = changeList.stream()
            .map(r -> buildVersionResource(r.getMessage().getRevision(),
                r.getMessage().getServiceProviderId(), r.getResource()))
            .collect(Collectors.toList());
        versionResources.addAll(versionResourceList);
      }
      // TODO /Andrew 29.03.17: fix this hack
      appendOrPutResources(store, versionsGraphKey, versionResources);
      versionResources.clear();
    }
  }

  private List<VersionResource> getVersionResourcesForKey(final URI versionsGraphKey,
      final Store store) {
    List<VersionResource> resources;
    resources = new ArrayList<>();
    try {
      if (store.namedGraphExists(versionsGraphKey)) {
        resources = store.getResources(versionsGraphKey, VersionResource.class);
      }
    } catch (StoreAccessException | ModelUnmarshallingException e) {
      log.error(String.valueOf(e));
    }
    return resources;
  }
}
