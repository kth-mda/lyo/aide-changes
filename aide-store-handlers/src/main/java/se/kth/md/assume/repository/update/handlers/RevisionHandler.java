package se.kth.md.assume.repository.update.handlers;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.tools.store.ModelUnmarshallingException;
import org.eclipse.lyo.tools.store.Store;
import org.eclipse.lyo.tools.store.StoreAccessException;
import org.eclipse.lyo.tools.store.update.Handler;
import org.eclipse.lyo.tools.store.update.ServiceProviderMessage;
import org.eclipse.lyo.tools.store.update.change.Change;
import org.eclipse.lyo.tools.store.update.change.ChangeHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.aide.diff.RevisionKeyProvider;
import se.kth.md.aide.repository.api.Repository;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.assume.repository.update.RevisionMessage;

/**
 * RevisionHandler is responsible for updating the state of the triplestore.
 *
 * <p>RevisionHandler saves every state of the resource. Current implementation puts all
 * resources from a revision into a separate named graph, leading to the resource duplication but
 * simplifies both the read and write operations.</p>
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-09-16
 */
public class RevisionHandler<T extends ServiceProviderMessage & RevisionMessage> implements
    Handler<T> {

  private final static Logger log = LoggerFactory.getLogger(RevisionHandler.class);
  private final RevisionKeyProvider keyProvider;
  private final Repository repository;

  public RevisionHandler(RevisionKeyProvider keyProvider, Repository repository) {
    this.keyProvider = keyProvider;
    this.repository = repository;
  }

  @Override
  public void handle(Store store, Collection<Change<T>> changes) {
    try {
      Map<Revision, List<Change<T>>> revisionChangeMap = groupBy(changes,
          o -> o.getMessage().getRevision());
      ArrayList<Revision> changeRevisions = new ArrayList<>(revisionChangeMap.keySet());
      changeRevisions.sort(Comparator.comparing(Revision::getTimestamp));
      for (Revision changeRevision : changeRevisions) {
        log.info("Handling changes for rev. {}", changeRevision);
        handleChangesPerRevision(store, changeRevision, revisionChangeMap.get(changeRevision));
      }
    } catch (Exception e) {
      // Handlers must not throw exceptions
      log.error("Cannot generate the Store key for the Revision Diff", e);
    }
  }

  private void handleChangesPerRevision(Store store, final Revision revision,
      final List<Change<T>> changes) throws ExecutionException {
    Map<URI, List<Change<T>>> changesPerServiceProvider = groupBy(changes,
        o -> o.getMessage().getServiceProviderId());
    for (Map.Entry<URI, List<Change<T>>> spEntry : changesPerServiceProvider.entrySet()) {
      handleChangesPerRevisionPerServiceProvider(store, revision, spEntry);
    }
  }

  private void handleChangesPerRevisionPerServiceProvider(Store store, Revision revision,
      Map.Entry<URI, List<Change<T>>> spEntry) throws ExecutionException {
    URI spId = spEntry.getKey();
    List<Change<T>> changesPerSP = spEntry.getValue();
    URI storeKeyPerRevisionPerFile = serviceProviderRevisionGraph(revision, spId);
    log.info("Saving the resources under the diff for {}", revision);

    Revision prevRevision = previousRevisionOf(repository, revision);
    URI prevRevisionSpGraph = serviceProviderRevisionGraph(prevRevision, spId);
    Map<? extends Class<? extends AbstractResource>, List<Change<T>>> resourceClassChangeMap =
        groupBy(
        changesPerSP, o -> o.getResource().getClass());
    for (Map.Entry<? extends Class<? extends AbstractResource>, List<Change<T>>> listEntry :
        resourceClassChangeMap
        .entrySet()) {
      Class<? extends AbstractResource> aClass = listEntry.getKey();
      List<Change<T>> changeList = listEntry.getValue();
      try {
        // TODO /Andrew 21.03.17: add a generic method to get all kinds of resources from
        // the model
        // probably need to do getModel(URI) and then use Jena Model helper by hand
        // TODO add JenaModelHelper static map of classes to automatically
        List<Change<T>> changesCreated = ChangeHelper.changesCreated(changeList);
        List<Change<T>> changesModified = ChangeHelper.changesModified(changeList);
        Map<URI, Change<T>> modifiedMap = modifiedResourceChangeMap(changesModified);
        List<AbstractResource> oldRevisionResources = changeResources(changesModified);
        try {
          // FIXME Andrew@2017-04-03: investigate why the namedGraphExists fails sometimes
          if (prevRevisionSpGraph != null && store.namedGraphExists(prevRevisionSpGraph)) {
            oldRevisionResources = new ArrayList<>(store.getResources(prevRevisionSpGraph, aClass));
          }
        } catch (IllegalArgumentException | StoreAccessException | ModelUnmarshallingException e) {
          // TODO /Andrew 22.03.17: refactor away the unchecked exceptions
          log.error("Error while parsing old revision resources", e);
        }
        replaceModifiedResources(oldRevisionResources, modifiedMap);
        List<AbstractResource> createdResources = changeResources(changesCreated);
        oldRevisionResources.addAll(createdResources);
        store.appendResources(storeKeyPerRevisionPerFile, oldRevisionResources);
      } catch (StoreAccessException e) {
        log.error("Error while inserting the resources into the triplestore", e);
      }
    }
  }

  /**
   * Use keyProvider directly
   */
  @Deprecated
  private URI serviceProviderRevisionGraph(final Revision revision, final URI serviceProvider) {
    if (revision != null) {
      // FIXME Andrew@2017-04-04: not an id! used only to show the intended usage of the
      // keyProvider.
      String serviceProviderId = serviceProvider.toString();
      // TODO Andrew@2017-04-04: KeyProvider lacks a method key(URI, Revision)
      //      URI wrongGraphUri = keyProvider.key(serviceProviderId, revision);

      URI heuristicGraphUri = URI.create(serviceProvider.toString() + '/' + revision.getId());
      return heuristicGraphUri;
    } else {
      return null;
    }
  }

  private void replaceModifiedResources(final List<AbstractResource> resources,
      final Map<URI, Change<T>> modifiedMap) {
    for (int i = 0; i < resources.size(); i++) {
      URI uri = resources.get(i).getAbout();
      if (modifiedMap.containsKey(uri)) {
        resources.set(i, modifiedMap.get(uri).getResource());
      }
    }
  }

  private Map<URI, Change<T>> modifiedResourceChangeMap(final List<Change<T>> changesModified)
      throws IllegalArgumentException {
    Map<URI, Change<T>> map = new HashMap<>(changesModified.size());
    for (Change<T> tChange : changesModified) {
      URI about = tChange.getResource().getAbout();
      if (map.putIfAbsent(about, tChange) != null) {
        throw new IllegalArgumentException("There is more than one change for Resource " + about);
      }
    }
    return map;
  }

  private static <Y, T> Map<Y, List<Change<T>>> groupBy(final Collection<Change<T>> changes,
      final Function<Change<T>, Y> changeRevisionFunction) {
    return changes.stream().collect(Collectors.groupingBy(changeRevisionFunction));
  }

  // TODO /Andrew 21.03.17: move to the Repository class
  private static Revision previousRevisionOf(final Repository repository, final Revision revision) {
    List<Revision> orderedRevisionList = revisionListOrdered(repository);
    int revisionIndex = orderedRevisionList.indexOf(revision);
    if (revisionIndex > 0) {
      return orderedRevisionList.get(revisionIndex - 1);
    } else {
      return null;
    }
  }

  // TODO /Andrew 21.03.17: move to the Repository class
  private static List<Revision> revisionListOrdered(final Repository repository) {
    List<Revision> revisionList = repository.getRevisions();
    revisionList.sort(Comparator.comparing(Revision::getTimestamp));
    return revisionList;
  }

  private static <T> List<AbstractResource> changeResources(List<Change<T>> changes) {
    return changes.stream().map(Change::getResource).collect(Collectors.toList());
  }
}
