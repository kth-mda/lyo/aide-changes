package se.kth.md.assume.repository.update.handlers;

import java.net.URI;
import java.util.Collection;
import java.util.List;
import org.eclipse.lyo.tools.store.Store;
import org.eclipse.lyo.tools.store.StoreAccessException;
import org.eclipse.lyo.tools.store.update.Handler;
import org.eclipse.lyo.tools.store.update.change.Change;
import org.eclipse.lyo.tools.store.update.change.ChangeHelper;
import org.eclipse.lyo.tools.store.update.change.HistoryResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.assume.repository.update.RevisionOSLCMessage;

/**
 * TrsDiffHandler is .
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-09-06
 */
public class TrsDiffHandler implements Handler<RevisionOSLCMessage> {
  private final static Logger log = LoggerFactory.getLogger(TrsDiffHandler.class);
  private final URI TRS_GRAPH_NAME;

  public TrsDiffHandler(final URI trsGraphName) {
    TRS_GRAPH_NAME = trsGraphName;
  }

  @Override
  public void handle(Store store, Collection<Change<RevisionOSLCMessage>> changes) {
    List<HistoryResource> historyDataList = ChangeHelper.historyFrom(changes);
    try {
      if (!store.namedGraphExists(TRS_GRAPH_NAME)) {
        store.putResources(TRS_GRAPH_NAME, historyDataList);
      } else {
        store.appendResources(TRS_GRAPH_NAME, historyDataList);
      }
    } catch (StoreAccessException e) {
      log.warn("Failed to insert VM history into Store", e);
    }
  }
}
