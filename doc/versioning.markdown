# Updating the adaptor to use the aide-changes `VersionResource`

In order to update the adaptor to use versioning for certain Service Providers, do the following changes:

1. Use `aide-changes` 0.0.9 or higher
2. Add custom resource classes to the `Application` class (3 declarations, see ProR adaptor for a sample code). Be sure to use the constants from the `ConfigurationManagementConstants`.
3. Copy the JSP files (at least `versionresource.jsp` and `versionresourcescollection.jsp` as well as the UI preview JSP) under the `/se/kth/md/aide/oslc/ccm` folder.
4. Update the `ProRConstants` reference in the JSP with the one from the generated adaptor.
5. Update the generated `ServiceProvidersFactory` for the Service Provider you want. Add the `VersionResourceService.class` to the list of the `RESOURCE_CLASSES`. WARNING: this change will be lost upon the regeneration. NOTE: you have to do this step for every kind of the Service Provider in your adaptor.
