package se.kth.md.aide.diff;

import java.util.List;
import java.util.Map;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import se.kth.md.aide.repository.api.Filter;
import se.kth.md.aide.repository.api.FilterAbstractFactory;
import se.kth.md.aide.repository.api.Repository;
import se.kth.md.aide.repository.api.RevisionAbstractObject;

/**
 * Interface to define extraction of OSLC Resources from a single file in the
 * {@link se.kth.md.aide.repository.api.Repository}.
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-09-15
 */
// TODO /Andrew 12.03.17: make abstract and pass Repository to ctor
public interface RepositoryResourceExtractor {
  /**
   *
   * @param filterFactory
   * @return
   */
  // better that you should only use a given FilterAbstractFactory to create such filters.
  Filter getFilter(final FilterAbstractFactory filterFactory);

  /**
   * Main method that is called for the extraction purpose.
   */
  Map<ServiceProvider, List<AbstractResource>> extractResources(
      RevisionAbstractObject revisionedObject, Repository repository);
}
