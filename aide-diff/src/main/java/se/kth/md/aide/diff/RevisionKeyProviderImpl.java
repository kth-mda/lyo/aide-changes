package se.kth.md.aide.diff;

import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.aide.repository.api.RevisionAbstractObject;

/**
 * Created on 06.03.17
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public class RevisionKeyProviderImpl implements RevisionKeyProvider {

  private final Map<String, ServiceProvider> serviceProviders = new HashMap<>();

  public RevisionKeyProviderImpl(Collection<ServiceProvider> serviceProviderCollection) {
    for (ServiceProvider serviceProvider : serviceProviderCollection) {
      serviceProviders.put(serviceProvider.getIdentifier(), serviceProvider);
    }
  }

  //  @Override
  //  public Collection<ServiceProvider> getServiceProviders() {
  //    return serviceProviders.values();
  //  }

  @Override
  public ServiceProvider resolveServiceProvider(final AbstractResource resource,
      final RevisionAbstractObject from) {
    String serviceProviderId = constructServiceProviderId(resource, from);
    if (serviceProviders.containsKey(serviceProviderId)) {
      return serviceProviders.get(serviceProviderId);
    }

    throw new IllegalArgumentException(
        "Could not resolve a ServiceProvider for " + resource + " and " + from);
  }

  @Override
  public URI resolveNamedGraph(final ServiceProvider serviceProvider, final Revision revision) {
    String serviceProviderUri = StringUtils.removeEnd(serviceProvider.getAbout().toString(), "/");
    return URI.create(serviceProviderUri + "/" + revision);
  }

  protected String constructServiceProviderId(final AbstractResource resource,
      final RevisionAbstractObject from) {
    return from.getPath();
  }
  //  @Override
  //  public URI key(final String path, final Revision currentRevision) {
  //    // TODO: 06.03.17 fix it with a better implementation, also use .resolve()
  //    String format = String.format("%s/%s", serviceProviderUri(path), currentRevision.getId());
  //    return URI.create(format);
  //  }
  //
  //  @Override
  //  public String getServiceProviderIdFor(final RevisionAbstractObject object) {
  //    return object.getPath();
  //  }
  //
  //  @Override
  //  public URI serviceProviderUri(final String serviceProvider) {
  //    return URI.create(String.format("http://store/%s", serviceProvider));
  //  }
}
