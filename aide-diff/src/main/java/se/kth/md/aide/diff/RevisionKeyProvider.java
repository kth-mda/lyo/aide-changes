package se.kth.md.aide.diff;

import java.net.URI;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.aide.repository.api.RevisionAbstractObject;

/**
 * Key provider with the following objectives:
 *
 * <ul>
 * <li>Provide a correct {@link org.eclipse.lyo.oslc4j.core.model.ServiceProvider} for an
 * extracted {@link org.eclipse.lyo.oslc4j.core.model.AbstractResource}</li>
 * <li>Provide a named graph URI for a resource under a given {@link Revision}</li>
 * </ul>
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public interface RevisionKeyProvider {
  ServiceProvider resolveServiceProvider(AbstractResource resource, RevisionAbstractObject from);

  URI resolveNamedGraph(ServiceProvider serviceProvider, Revision revision);
}
