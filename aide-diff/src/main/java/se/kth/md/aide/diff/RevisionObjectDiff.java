package se.kth.md.aide.diff;

import com.google.common.collect.Sets;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import se.kth.md.aide.repository.api.RevisionAbstractObject;

/**
 * RevisionObjectDiff is .
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-06-28
 */
public class RevisionObjectDiff {

  private final Set<UriResourceWrapper> revOldResources;
  private final Set<UriResourceWrapper> revCurrentResources;
  private final RevisionAbstractObject revCurrent;
  private Set<AbstractResource> addedResources;
  private Set<AbstractResource> deletedResources;

  public RevisionObjectDiff(List<? extends AbstractResource> revOldResources,
      List<? extends AbstractResource> revCurrentResources, RevisionAbstractObject revCurrent) {
    this.revOldResources = resourceSet(revOldResources);
    this.revCurrentResources = resourceSet(revCurrentResources);
    this.revCurrent = revCurrent;
  }

  private Set<UriResourceWrapper> resourceSet(List<? extends AbstractResource> resources) {
    return resources.stream().map(UriResourceWrapper::new).collect(Collectors.toSet());
  }

  public RevisionAbstractObject getRevision() {
    return revCurrent;
  }

  public Set<? extends AbstractResource> getCreatedResources() {
    if (addedResources == null) {
      Set<UriResourceWrapper> addedSet = Sets.difference(revCurrentResources, revOldResources)
        .immutableCopy();
      addedResources = unWrap(addedSet);
    }
    return addedResources;
  }

  private Set<AbstractResource> unWrap(Set<UriResourceWrapper> addedSet) {
    return addedSet.stream().map(UriResourceWrapper::getResource).collect(Collectors.toSet());
  }

  public Set<AbstractResource> getDeletedResources() {
    if (deletedResources == null) {
      Set<UriResourceWrapper> deletedSet = Sets.difference(revOldResources, revCurrentResources)
        .immutableCopy();
      deletedResources = unWrap(deletedSet);
    }
    return deletedResources;
  }

  public Set<AbstractResource> getModifiedResources() {
    Set<UriResourceWrapper> wrapperOld = Sets.intersection(revOldResources, revCurrentResources)
        .immutableCopy();
    Set<AbstractResource> commonResourcesOld = new HashSet<>(unWrap(wrapperOld));
    Set<UriResourceWrapper> wrapperNew = Sets.intersection(revCurrentResources, revOldResources)
        .immutableCopy();
    Set<AbstractResource> commonResourcesNew = new HashSet<>(unWrap(wrapperNew));
    Set<AbstractResource> newModifiedResources = Sets.difference(commonResourcesNew,
        commonResourcesOld).immutableCopy();
    return newModifiedResources;
  }

}

