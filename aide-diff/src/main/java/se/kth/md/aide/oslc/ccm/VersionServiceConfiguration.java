package se.kth.md.aide.oslc.ccm;

import com.google.common.base.Strings;
import java.net.URI;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.lyo.tools.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.aide.diff.RevisionKeyProvider;

/**
 * Static global vars will blow up in your face one day!!!
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.8
 */
public class VersionServiceConfiguration {
  private final static Logger log = LoggerFactory.getLogger(VersionServiceConfiguration.class);
  private static final String VERSIONS_URI_CONST = "versions";
  private static final Map<String, ServiceProvider> serviceProviders = new HashMap<>();
  /**
   * Use the getters & setters
   */
  // TODO /Andrew 2017-04-03: inject/pass
  private static RevisionKeyProvider keyProvider;
  /**
   * Use the getters & setters
   */
  // TODO /Andrew 2017-04-03: inject/pass
  private static Store store;
  /**
   * Use the getters & setters
   */
  // TODO /Andrew 2017-04-03: inject/pass
  private static String servicesBase;

  public static RevisionKeyProvider getKeyProvider() {
    return keyProvider;
  }

  public static void setKeyProvider(final RevisionKeyProvider keyProvider) {
    VersionServiceConfiguration.keyProvider = keyProvider;
  }

  public static void setServiceProviders(Collection<ServiceProvider> serviceProviderCollection) {
    for (ServiceProvider serviceProvider : serviceProviderCollection) {
      serviceProviders.put(serviceProvider.getIdentifier(), serviceProvider);
    }
  }

  public static Optional<ServiceProvider> findServiceProviderById(String serviceProviderId) {
    return Optional.ofNullable(serviceProviders.getOrDefault(serviceProviderId, null));
  }

  public static Store getStore() {
    if (store == null) {
      log.error("VersionServiceConfiguration.setStore() was not called");
    }
    return store;
  }

  public static void setStore(final Store store) {
    VersionServiceConfiguration.store = store;
  }

  public static String getServicesBase() {
    if (Strings.isNullOrEmpty(servicesBase)) {
      log.error("VersionServiceConfiguration.setServicesBase() was not called");
    }

    return servicesBase;
  }

  public static void setServicesBase(final String servicesBase) {
    VersionServiceConfiguration.servicesBase = servicesBase;
  }

  public static URI getVersionResourceGraphName(URI serviceProvider) {
    if (serviceProvider == null) {
      throw new IllegalArgumentException("Service Provider URI cannot be NULL");
    }
    URI uri = URI.create(serviceProvider.toString() + '/' + VERSIONS_URI_CONST);
    log.debug("Graph name for '{}': '{}'", serviceProvider, uri);
    return uri;
  }

  public static URI getVersionResourceGraphName(ServiceProvider serviceProvider) {
    return getVersionResourceGraphName(serviceProvider.getAbout());
  }
}
