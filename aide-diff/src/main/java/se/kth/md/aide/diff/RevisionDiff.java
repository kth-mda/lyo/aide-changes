package se.kth.md.aide.diff;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.aide.repository.api.Repository;
import se.kth.md.aide.repository.api.RevisionAbstractObject;

/**
 * RevisionDiff is .
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-09-02
 */
public class RevisionDiff {

  private final Logger logger = LoggerFactory.getLogger(getClass());
  private final Repository repository;
  private final RepositoryResourceExtractor extractor;
  private final Map<ServiceProvider, Collection<RevisionObjectDiff>> resourceDiffs = new
      HashMap<>();
  private final RevisionKeyProvider keyProvider;
  private List<RevisionAbstractObject> oldRevisionFiles;
  private List<RevisionAbstractObject> newRevisionFiles;

  public RevisionDiff(final Repository repository, final RepositoryResourceExtractor extractor,
      final RevisionKeyProvider keyProvider, final List<RevisionAbstractObject> oldRevisionFiles,
      final List<RevisionAbstractObject> newRevisionFiles) {
    this.repository = repository;
    this.extractor = extractor;
    this.keyProvider = keyProvider;
    this.oldRevisionFiles = oldRevisionFiles;
    this.newRevisionFiles = newRevisionFiles;
  }

  @SuppressWarnings("unused")
  public void performDiff() throws IOException {
    final ImmutableSet<RevisionAbstractObjectWrapper> oldFiles = ImmutableSet.copyOf(
        wrap(oldRevisionFiles));
    final ImmutableSet<RevisionAbstractObjectWrapper> newFiles = ImmutableSet.copyOf(
        wrap(newRevisionFiles));

    final Sets.SetView<RevisionAbstractObjectWrapper> deletedFiles = Sets.difference(oldFiles,
        newFiles);
    final Sets.SetView<RevisionAbstractObjectWrapper> addedFiles = Sets.difference(newFiles,
        oldFiles);

    final Sets.SetView<RevisionAbstractObjectWrapper> oldCommonFiles = Sets.intersection(oldFiles,
        newFiles);
    final Sets.SetView<RevisionAbstractObjectWrapper> newCommonFiles = Sets.intersection(newFiles,
        oldFiles);

    appendMapDiffs(compareEachCommonFile(unwrap(oldCommonFiles), unwrap(newCommonFiles)));
    appendMapDiffs(deletedResourceDiffs(unwrap(deletedFiles)));
    appendMapDiffs(createdResourceDiffs(unwrap(addedFiles)));
  }

  public Map<ServiceProvider, Collection<RevisionObjectDiff>> getResourceDiffs() {
    return resourceDiffs;
  }

  public RevisionKeyProvider getKeyProvider() {
    return keyProvider;
  }

  private void appendMapDiffs(final Map<ServiceProvider, Collection<RevisionObjectDiff>> diffMap) {
    for (ServiceProvider serviceProvider : diffMap.keySet()) {
      Collection<RevisionObjectDiff> diffs = resourceDiffs.getOrDefault(serviceProvider,
          new ArrayList<>());
      diffs.addAll(diffMap.get(serviceProvider));
      if (!resourceDiffs.containsKey(serviceProvider)) {
        resourceDiffs.put(serviceProvider, diffs);
      }
    }
  }

  private List<RevisionAbstractObjectWrapper> wrap(Collection<RevisionAbstractObject> objects) {
    List<RevisionAbstractObjectWrapper> wrapperList = objects.stream().map(
        RevisionAbstractObjectWrapper::new).collect(Collectors.toList());
    return wrapperList;
  }

  private Set<RevisionAbstractObject> unwrap(Collection<RevisionAbstractObjectWrapper> objects) {
    Set<RevisionAbstractObject> unwrapped = objects.stream().map(
        RevisionAbstractObjectWrapper::getRevisionAbstractObject).collect(Collectors.toSet());
    return unwrapped;
  }

  private Map<ServiceProvider, Collection<RevisionObjectDiff>> createdResourceDiffs(
      Set<RevisionAbstractObject> addedFiles) {
    HashSet<RevisionAbstractObject> revisionedObjects = Sets.newHashSet(addedFiles);
    // TODO Andrew@2017-05-03: refactor to deduplicate
    Map<ServiceProvider, Collection<RevisionObjectDiff>> revDiffs = new HashMap<>();
    for (RevisionAbstractObject revObj : revisionedObjects) {
      Map<ServiceProvider, List<AbstractResource>> revCurrentResources = extractResources(revObj);
      for (Map.Entry<ServiceProvider, List<AbstractResource>> entry : revCurrentResources
          .entrySet()) {
        ServiceProvider serviceProvider = entry.getKey();
        Collection<RevisionObjectDiff> list = revDiffs.getOrDefault(serviceProvider,
            new ArrayList<>());
        list.add(new RevisionObjectDiff(new ArrayList<>(), entry.getValue(), revObj));
        if (!revDiffs.containsKey(serviceProvider)) {
          revDiffs.put(serviceProvider, list);
        }
      }
    }
    return revDiffs;
  }

  private Map<ServiceProvider, Collection<RevisionObjectDiff>> deletedResourceDiffs(
      Set<RevisionAbstractObject> deletedFiles) {
    HashSet<RevisionAbstractObject> revisionedObjectHashSet = Sets.newHashSet(deletedFiles);
    Map<ServiceProvider, Collection<RevisionObjectDiff>> revDiffs = new HashMap<>();
    for (RevisionAbstractObject revObj : revisionedObjectHashSet) {
      Map<ServiceProvider, List<AbstractResource>> resourcesPerSP = extractResources(revObj);
      for (Map.Entry<ServiceProvider, List<AbstractResource>> entry : resourcesPerSP.entrySet()) {
        ServiceProvider serviceProvider = entry.getKey();
        Collection<RevisionObjectDiff> list = revDiffs.getOrDefault(serviceProvider,
            new ArrayList<>());
        list.add(new RevisionObjectDiff(entry.getValue(), new ArrayList<>(), revObj));
        if (!revDiffs.containsKey(serviceProvider)) {
          revDiffs.put(serviceProvider, list);
        }
      }
    }
    return revDiffs;
  }

  private Map<ServiceProvider, Collection<RevisionObjectDiff>> compareEachCommonFile(
      Set<RevisionAbstractObject> revSetOld, Set<RevisionAbstractObject> revSetNew)
      throws IOException {
    Map<ServiceProvider, Collection<RevisionObjectDiff>> fileDiffs = new HashMap<>();
    for (RevisionAbstractObject oldObj : revSetOld) {
      Optional<RevisionAbstractObject> revisionedObjectOptional = revSetNew.stream().filter(
          rObj -> rObj.getPath().equals(oldObj.getPath())).findFirst();

      if (revisionedObjectOptional.isPresent()) {
        RevisionAbstractObject newObj = revisionedObjectOptional.get();
        logger.debug("Comparing {} & {}", oldObj, newObj);
        Map<ServiceProvider, List<AbstractResource>> oldResources = extractResources(oldObj);
        Map<ServiceProvider, List<AbstractResource>> newResources = extractResources(newObj);
        Set<ServiceProvider> oldKeys = oldResources.keySet();
        Set<ServiceProvider> newKeys = newResources.keySet();
        Sets.SetView<ServiceProvider> allKeys = Sets.union(oldKeys, newKeys);
        for (ServiceProvider serviceProvider : allKeys) {
          RevisionObjectDiff diff = new RevisionObjectDiff(oldResources.get(serviceProvider),
              newResources.get(serviceProvider), newObj);
          Collection<RevisionObjectDiff> objectDiffs = fileDiffs.getOrDefault(serviceProvider,
              new ArrayList<>());
          objectDiffs.add(diff);
          if (!fileDiffs.containsKey(serviceProvider)) {
            fileDiffs.put(serviceProvider, objectDiffs);
          }
        }

        // TODO Andrew@2017-05-03: either do a diff per SP or do the diff

      } else {
        throw new IllegalArgumentException(
            "File-level diff works only on the intersection of file lists");
      }
    }
    return fileDiffs;
  }

  private Map<ServiceProvider, List<AbstractResource>> extractResources(
      RevisionAbstractObject revisionedObject) {
    return extractor.extractResources(revisionedObject, repository);
  }

  class RevisionAbstractObjectWrapper {
    private final RevisionAbstractObject revisionAbstractObject;

    public RevisionAbstractObjectWrapper(RevisionAbstractObject revisionAbstractObject) {
      this.revisionAbstractObject = revisionAbstractObject;
    }

    @Override
    public int hashCode() {
      return Objects.hash(revisionAbstractObject.getPath());
    }

    @Override
    public boolean equals(final Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      final RevisionAbstractObjectWrapper that = (RevisionAbstractObjectWrapper) o;
      return Objects.equals(revisionAbstractObject.getPath(),
          that.revisionAbstractObject.getPath());
    }

    public RevisionAbstractObject getRevisionAbstractObject() {
      return revisionAbstractObject;
    }
  }
}
