package se.kth.md.aide.diff;

import java.util.Objects;
import org.eclipse.lyo.oslc4j.core.model.AbstractResource;

// TODO: 21.02.17 javadoc
public class UriResourceWrapper {
  private final AbstractResource resource;

  public UriResourceWrapper(AbstractResource resource) {
    this.resource = resource;
  }

  @Override
  public int hashCode() {
    return Objects.hash(resource.getAbout());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UriResourceWrapper that = (UriResourceWrapper) o;
    return Objects.equals(resource.getAbout(), that.resource.getAbout());
  }

  public AbstractResource getResource() {
    return resource;
  }
}
