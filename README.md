# AIDE support libraries for OSLC resource changes
 
## Contents

* `aide-diff` allows OSLC-resource-level diff (requires `equals()` support on the OSLC resources).
* `aide-changeprovider` contains the diff-based `ChangeProvider` for use with `lyo-store-update` (i.e. the update part of the `lyo-store`).
* `aide-store-handlers` contains extra handlers for changes relevant to repository-based adaptors.

## Documentation

* [**Adding Versioning Service to the select Service Providers**](doc/versioning.markdown)

## License

> Copyright 2023 KTH Royal Institute of Technology
> 
> Licensed under the EUPL-1.2-or-later.
