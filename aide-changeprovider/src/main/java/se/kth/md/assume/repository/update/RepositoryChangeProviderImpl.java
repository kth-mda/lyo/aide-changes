package se.kth.md.assume.repository.update;

import java.io.IOException;
import java.net.URI;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.eclipse.lyo.oslc4j.core.model.ServiceProvider;
import org.eclipse.lyo.tools.store.update.change.Change;
import org.eclipse.lyo.tools.store.update.change.ChangeKind;
import org.eclipse.lyo.tools.store.update.change.ChangeProvider;
import org.eclipse.lyo.tools.store.update.change.HistoryResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.aide.diff.RepositoryResourceExtractor;
import se.kth.md.aide.diff.RevisionDiff;
import se.kth.md.aide.diff.RevisionKeyProvider;
import se.kth.md.aide.diff.RevisionObjectDiff;
import se.kth.md.aide.repository.api.Filter;
import se.kth.md.aide.repository.api.Repository;
import se.kth.md.aide.repository.api.RepositoryAccessException;
import se.kth.md.aide.repository.api.Revision;
import se.kth.md.aide.repository.api.RevisionAbstractObject;

/**
 * RepositoryChangeProviderImpl is .
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $Id$
 * @since 0.0.0
 */
public class RepositoryChangeProviderImpl implements ChangeProvider<RevisionOSLCMessage> {
  private final static Logger log = LoggerFactory.getLogger(RepositoryChangeProviderImpl.class);
  private final Repository repository;
  private final List<RepositoryResourceExtractor> extractors;
  private final RevisionKeyProvider keyProvider;

  public RepositoryChangeProviderImpl(Repository repository,
    List<RepositoryResourceExtractor> extractors, RevisionKeyProvider keyProvider) {
    this.repository = repository;
    this.extractors = extractors;
    this.keyProvider = keyProvider;
  }

  @Override
  public Collection<Change<RevisionOSLCMessage>> getChangesSince(ZonedDateTime since,
    RevisionOSLCMessage revisionMessage) {
    List<Revision> revisionList = repository.getRevisions();
    revisionList.sort(Comparator.comparing(Revision::getTimestamp));
    Collection<Change<RevisionOSLCMessage>> changes = new ArrayList<>();

    // Can't use Collections.binarySearch, replace it with an efficient search if needed
    Revision oldRevision = Revision.dummy();

    List<Filter> filters = new ArrayList<>(extractors.size());
    for (RepositoryResourceExtractor extractor : extractors) {
      filters.add(extractor.getFilter(repository.getFilterFactory()));
    }

    List<List<RevisionAbstractObject>> oldRevList = new ArrayList<>();
    for (int f = 0; f < filters.size(); f++) {
      oldRevList.add(new ArrayList<>());
    }

    for (int i = 0; i < revisionList.size(); i++) {
      Revision currentRevision = revisionList.get(i);
      log.trace("Revision {} found", currentRevision);
      if (currentRevision.getTimestamp().isAfter(since)) {
        for (int f = 0; f < filters.size(); f++) {
          Filter filter = filters.get(f);
          RepositoryResourceExtractor extractor = extractors.get(f);
          List<RevisionAbstractObject> oldRevisionFiles = oldRevList.get(f);
          try {
            List<RevisionAbstractObject> currentRevObjects = getRevisionFilesOrEmpty(
              currentRevision, repository, filter);

            // TODO /Andrew 12.03.17: makes no sense as every file could have its own SP
            RevisionDiff diff = new RevisionDiff(repository, extractor, keyProvider,
              oldRevisionFiles, currentRevObjects);
            diff.performDiff();
            Map<ServiceProvider, Collection<RevisionObjectDiff>> addDiffs = diff.getResourceDiffs();
            changes.addAll(changesFromDiff(addDiffs));
            oldRevList.set(f, currentRevObjects);
          } catch (IOException e) {
            log.error("Error occurred during the repository object diff", e);
          }
        }
      }
    }
    log.debug("Returning {} resources (message='{}')", changes.size(),
      String.valueOf(revisionMessage));
    return changes;
  }

  private List<RevisionAbstractObject> getRevisionFilesOrEmpty(Revision oldRevision,
    Repository repository, final Filter filter) throws RepositoryAccessException {
    List<RevisionAbstractObject> oldRevisionFiles;
    if (oldRevision.getId() == null) {
      // dummy revision
      oldRevisionFiles = new ArrayList<>();
    } else {
      oldRevisionFiles = repository.getRevisionObjects(oldRevision, filter);
    }
    return oldRevisionFiles;
  }

  //  @Deprecated
  private Collection<Change<RevisionOSLCMessage>> changesFromDiff(
      Map<ServiceProvider, Collection<RevisionObjectDiff>> diffs) {
    //    return diffs.stream()
    //      .flatMap(diff -> fromResourceDiff(diff).stream())
    //      .collect(Collectors.toList());
    final Collection<Change<RevisionOSLCMessage>> historyData = new ArrayList<>();
    for (Map.Entry<ServiceProvider, Collection<RevisionObjectDiff>> entry : diffs.entrySet()) {
      Collection<RevisionObjectDiff> revisionObjectDiffs = entry.getValue();
      for (RevisionObjectDiff diff : revisionObjectDiffs) {
        RevisionAbstractObject revisionAbstractObject = diff.getRevision();
        // TODO /Andrew 12.03.17: keyProvider.key(revisionObject)
        //        URI serviceProviderId = keyProvider.serviceProviderUri(diff.getRevision());
        ServiceProvider serviceProvider = entry.getKey();
        final URI serviceProviderId = serviceProvider.getAbout();
        Revision revision = revisionAbstractObject.getRevision();
        RevisionOSLCMessage message = new RevisionOSLCMessage(serviceProviderId, revision);

        historyData.addAll(diff.getCreatedResources()
            .stream()
            .map(r -> new Change<>(r,
                new HistoryResource(ChangeKind.CREATION, revision.getTimestamp(), r.getAbout()),
                message))
            .collect(Collectors.toList()));
        historyData.addAll(diff.getDeletedResources()
            .stream()
            .map(r -> new Change<>(r,
                new HistoryResource(ChangeKind.DELETION, revision.getTimestamp(), r.getAbout()),
                message))
            .collect(Collectors.toList()));
        historyData.addAll(diff.getModifiedResources()
            .stream()
            .map(r -> new Change<>(r,
                new HistoryResource(ChangeKind.MODIFICATION, revision.getTimestamp(), r.getAbout()),
                message))
            .collect(Collectors.toList()));

      }
    }
    return historyData;
  }

  //  @Deprecated
  //  protected Collection<Change<RevisionOSLCMessage>> fromResourceDiff(
  //    RevisionObjectDiff diff) {
  //    RevisionAbstractObject revisionAbstractObject = diff.getRevision();
  //    List<Change<RevisionOSLCMessage>> historyData = new ArrayList<>();
  //
  //    // TODO /Andrew 12.03.17: keyProvider.key(revisionObject)
  //    URI serviceProviderId = keyProvider.serviceProviderUri(diff.getRevision());
  //    Revision revision = revisionAbstractObject.getRevision();
  //    RevisionOSLCMessage message = new RevisionOSLCMessage(serviceProviderId, revision);
  //
  //    historyData.addAll(diff.getCreatedResources()
  //      .stream()
  //      .map(r -> new Change<>(r,
  //        new HistoryResource(ChangeKind.CREATION, revision.getTimestamp(), r.getAbout()),
  // message))
  //      .collect(Collectors.toList()));
  //    historyData.addAll(diff.getDeletedResources()
  //      .stream()
  //      .map(r -> new Change<>(r,
  //        new HistoryResource(ChangeKind.DELETION, revision.getTimestamp(), r.getAbout()),
  // message))
  //      .collect(Collectors.toList()));
  //    historyData.addAll(diff.getModifiedResources()
  //      .stream()
  //      .map(r -> new Change<>(r,
  //        new HistoryResource(ChangeKind.MODIFICATION, revision.getTimestamp(), r.getAbout()),
  //        message))
  //      .collect(Collectors.toList()));
  //    return historyData;
  //  }
}
