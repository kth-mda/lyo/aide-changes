package se.kth.md.assume.repository.update;

import se.kth.md.aide.repository.api.Revision;

/**
 * Created on 06.03.17
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $version-stub$
 * @since 0.0.1
 */
public interface RevisionMessage {
  Revision getRevision();
}
