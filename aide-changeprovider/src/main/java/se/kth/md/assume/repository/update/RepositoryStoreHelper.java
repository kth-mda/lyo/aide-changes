package se.kth.md.assume.repository.update;

import org.eclipse.lyo.oslc4j.core.model.AbstractResource;
import org.eclipse.lyo.tools.store.Store;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se.kth.md.aide.diff.RevisionKeyProvider;
import se.kth.md.aide.repository.api.Repository;
import se.kth.md.aide.repository.api.RepositoryAccessException;

/**
 * Allows retrieval of the OSLC Resources stored in the {@link Store}.
 * <p>Typically OSLC Resources are inserted into {@link Store} with help of
 * {@link org.eclipse.lyo.tools.store.update.StoreUpdateManager}.</p>
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @since 2016-06-13
 */
public class RepositoryStoreHelper<R extends AbstractResource> {
  private final Logger logger = LoggerFactory.getLogger(RepositoryStoreHelper.class);
  private final Repository repository;
  private final Store store;
  private final RevisionKeyProvider keyProvider;
  private final Class<R> clazz;

  public RepositoryStoreHelper(Repository repository, Store store, RevisionKeyProvider keyProvider,
    Class<R> clazz)
    throws RepositoryAccessException {
    this.repository = repository;
    this.store = store;
    this.keyProvider = keyProvider;
    this.clazz = clazz;
  }

  //  public List<R> getCurrentResources(String serviceProviderId)
  //    throws StoreAccessException, IOException, ExecutionException, ModelUnmarshallingException {
  //    return getResources(serviceProviderId, getCurrentRevision());
  //  }

  //  public Revision getCurrentRevision() {
  //    List<Revision> revisions = repository.getRevisions();
  //    revisions.sort(Comparator.comparing(Revision::getTimestamp));
  //    return revisions.get(revisions.size() - 1);
  //  }

  //  public List<R> getResources(String serviceProviderId, Revision revision)
  //    throws StoreAccessException, IOException, ExecutionException, ModelUnmarshallingException {
  //    List<R> resources = new ArrayList<>();
  //    URI cacheKey = keyProvider.key(serviceProviderId, revision);
  //    logger.trace("Checking if {} is in the Adapter Triplestore", cacheKey);
  //    if (store.namedGraphExists(cacheKey)) {
  //      try {
  //        resources = store.getResources(cacheKey, clazz);
  //        logger.trace("{} resources were present in the Adapter Triplestore (size={})", cacheKey,
  //          resources.size());
  //      } catch (StoreAccessException | ModelUnmarshallingException e) {
  //        logger.error(String.valueOf(e));
  //      }
  //    } else {
  //      logger.info("Cache key '{}' was missing from the Adapter Triplestore. Existing keys:",
  //        cacheKey);
  //      try {
  //        for (String key : store.keySet()) {
  //          logger.trace("\t{}", key);
  //        }
  //      } catch (UnsupportedOperationException e) {
  //        logger.warn("Store doesn't support the 'store.keySet()' operation");
  //      }
  //    }
  //    return resources;
  //  }

}
