package se.kth.md.assume.repository.update;

import com.google.common.base.MoreObjects;
import java.net.URI;
import org.eclipse.lyo.tools.store.update.OSLCMessage;
import se.kth.md.aide.repository.api.Revision;

/**
 * RevisionMessage is .
 * @author Andrew Berezovskyi (andriib@kth.se)
 * @version $Id$
 * @since 0.0.0
 */
public class RevisionOSLCMessage extends OSLCMessage implements RevisionMessage {
  private final Revision revision;

  public RevisionOSLCMessage(URI serviceProviderId, Revision revision) {
    super(serviceProviderId);
    this.revision = revision;
  }

  @Override
  public Revision getRevision() {
    return revision;
  }

  @Override
  public String toString() {
    return MoreObjects.toStringHelper(this).add("revision", revision).toString();
  }
}
